const express = require ('express');

const router = express.Router ();

const courseController = require ('../controllers/courseControllers')

const auth = require ('../auth')

//1. create a course DONE
router.post ('/create-course', auth.verify, (req, res) => {
	courseController.createCourse (req.body).then (result => res.send (result))
})


//2. retrieving all courses DONE
router.get ('/', (req, res) => {
	courseController.getAllCourses().then ( result =>res.send (result))
})

//3. retrieving only active courses DONE
router.get ('/active-courses', (req, res) => {
	courseController.getActiveCourses(req.body).then(result => res.send (result))
})


//4. Get a specific course using findOne()
router.get('/specific-course', auth.verify, (req, res) => {
	// console.log(req.body)	//object
	courseController.getSpecificCourse(req.body.courseName).then( result => res.send(result))
})

//5. Get specific course using findById()

router.get("/:courseId", auth.verify, (req, res) => {

	let paramsId = req.params.courseId
	courseController.getCourseById(paramsId).then(result => res.send(result))
})


//6. Update isActive status of the course using findOneAndUpdate()
	//update isActive status to false
router.put("/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.body.courseName).then( result => res.send(result))
})


//7. Update isActive status to true
router.put("/unarchive", auth.verify, (req, res) => {

	courseController.unarchiveCourse(req.body.courseName).then( result => res.send(result))
})

//8. Update isActive status to true
router.put("/:courseId/unarchive", auth.verify, (req, res) => {

	courseController.unarchiveCourseById(req.params.courseId).then( result => res.send(result))
})


//9. Update isActive status of the course using findByIdAndUpdate()
	//update isActive status to false
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourseById(req.params.courseId).then(result => res.send(result))
})


//10. Delete course using findOneAndDelete()
router.delete("/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourse(req.body.courseName).then(result => res.send(result))
})


//11. Delete course using findByIdAndDelete()
router.delete("/:courseId/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourseById(req.params.courseId).then(result => res.send(result))
})

//12. Edit a course using findByIdAndUpdate
router.put("/:courseId/edit", auth.verify, (req, res) => {

	courseController.editCourse(req.params.courseId, req.body).then( result => res.send(result))
})



module.exports = router;
