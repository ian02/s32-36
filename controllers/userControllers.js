//BUSINESS LOGICS including model methods

//User Model
const User = require ('../models/User')
const auth = require ('../auth')

const bcrypt = require ('bcrypt')

module.exports.checkEmail = (email) => {
	return User.findOne ({email: email}).then((result, error) => {
		if (result !== null) {
			return (false)
		} else {

			if (result === null){
				return (true)
			} else {
				return (error)
			}
		}
	})
}
//checkEmail(<arguments>)

module.exports.register = (reqBody) => {
	//save/create a new user document
		//using .save() method to save document to the database

	//console.log (reqBody)

	//how to use object destructuring
		//why? to make distinct variables for each property w/o using dot notation
		//const {properties} = <object reference>
	const {firstName, lastName, email, password, mobileNo, age} = reqBody
		//console.log(firstName)

	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		password: bcrypt.hashSync(password, 10),
		age: age
	})

	//save the newUser object to the database
	return newUser.save().then((result, error) => {
		//console.log (result) //document

		if (result){
			return true
		} else {
			return error
		}
	})


}

//Mini Activity
//Get All Users
module.exports.getAllUsers = () => {
	//difference between findOne () and find() methods
		//findOne () returns one document
		//find(query, fieldprojection) returns an arraw of documents []
	return User.find ().then ((result, error) => {
		if (result !== null){
			return result
		} else {
			return error;
		}
	})
} 




module.exports.login = (reqBody) => {

	const {email, password} = reqBody

	return User.findOne({email: email}).then ((result, error) => {
		//console.log (result)

		if (result == null){
			// console.log ('email null');
			return false
		} else {

			let isPwCorrect = bcrypt.compareSync(password, result.password)

			if (isPwCorrect == true){

				//return json web token
				//invoke the function which creates the token upon logging in
				//requirements for creating a token:
					//if password matches from existing pw from db

					return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
			
		}
	})
}



module.exports.getUserProfile = (reqBody) => {
	//return User.findOne ({email: })

	/*mini activity*/
	//using findById method, look for the matching 

	return User.findById({_id: reqBody.id}).then(result => {
		return result;
	})
}


module.exports.editProfile = (userId, reqBody) => {

	return User.findByIdAndUpdate (userId, reqBody, {new:true}).then (result => {
		
		result.password="****"
		return result
	})
}

module.exports.editUser = (userId, reqBody) => {
	//console.log (userId)
	//console.log (reqBody)

	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}
	return User.findByIdAndUpdate(userId, updatedUser, {new:true}).then (result => {
		result.password = "*****"
		return result
	})
}


module.exports.editDetails = (reqBody) => {
	//console.log (reqBody)

	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}

	return User.findOneAndUpdate ({email: email}, updatedUser, {returnDocument:'after'}).then (result => {
		//console.log (result)
		result.password = "****"

		return result
	})

}

module.exports.delete = (userId) => {
	return User.findByIdAndDelete(userId).then (result => {
		if (result){
			return true
		}
	})

}

module.exports.deleteUser = (email) => {
	//console.log (email)

	return User.findOneAndDelete ({email:email}).then (result => {
		if (result){
			return true
		}
	})
}

//Enroll
module.exports.enroll = async (data) => {
	const {userId, courseId} = data


	//look for matching document of a user
	const userEnroll = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			// console.log(result)
			result.enrollments.push({courseId: courseId})

			return result.save().then( result => {
				return true
			})
		}

	})

	//look for matching document of a user
	const courseEnroll = await Course.findById(courseId).then( (result, err) => {
		if(err){
			return err
		} else {

			result.enrollees.push({userId: userId})

			return result.save().then( result => {
				return true
			})
		}
	})

	//to return only one value for the function enroll

	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}







