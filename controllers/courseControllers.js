const Course = require ('../models/Course');
const auth = require ('../auth');

const bcrypt = require ('bcrypt')

//1. create a course DONE
module.exports.createCourse = (reqBody) => {

	const {courseName, description, price} = reqBody

	const newCourse = new Course ({
		courseName: courseName,
		description: description,
		price: price
	})

	return newCourse.save().then ((result, error) => {
		if (result){
			return true
		} else {
			return error
		}
	})
}

//2. retrieving all courses
module.exports.getAllCourses = () => {
	return Course.find().then ((result, error) => {
		if (result !==null){
			return result
		} else {
			return error
		}
	})
}

//3. retrieving only active courses DONE
module.exports.getActiveCourses = (reqBody) => {
	return Course.find({ isActive: true }).then ((result, error) => {
		return result
	})
}

//4. Get a specific course using findOne()
module.exports.getSpecificCourse = (courseName) => {
	return Course.findOne({courseName: courseName}).then ((result, error) => {
		return result
	})
}


//5. Get specific course using findById()
module.exports.getCourseById = (courseId) => {
	return Course.findById(courseId).then (result => {
		return result
	})
}


//6. Update isActive status of the course using findOneAndUpdate()
module.exports.archiveCourse = (courseName, reqBody) => {
	return Course.findOneAndUpdate ({courseName: courseName}, { isActive: false }, {returnDocument:'after'}).then ((result, error) => {
		return result
	})
}


//7. Update isActive status to true
module.exports.unarchiveCourse = (courseName, reqBody) => {
	return Course.findOneAndUpdate ({courseName: courseName}, { isActive: true }, {returnDocument:'after'}).then ((result, error) => {
		return result
	})
}


//8. Update isActive status to true
module.exports.unarchiveCourseById = (courseId) => {
	return Course.findByIdAndUpdate (courseId, { isActive: true }, {returnDocument:'after'}).then ((result, error) => {
		return result
	})
}


//9. Update isActive status of the course using findByIdAndUpdate()
module.exports.archiveCourseById = (courseId) => {
	return Course.findByIdAndUpdate (courseId, { isActive: false }, {returnDocument:'after'}).then ((result, error) => {
		return result
	})
}


//10. Delete course using findOneAndDelete()
module.exports.deleteCourse = (courseName, reqBody) =>  {
	return Course.findOneAndDelete ({courseName: courseName}).then ((result, error) => {
		if (result){
			return true
		} else {
			return error
		}
	})
}



//11. Delete course using findByIdAndDelete()
module.exports.deleteCourseById = (courseId) => {
	return Course.findByIdAndDelete (courseId).then ((result, error ) => {
		if (result) {
			return ('Course deleted.')
		} else {
			return error
		}
	})
}

//12. Edit a course using findByIdAndUpdate
module.exports.editCourse = (courseId, reqBody) => {
	return Course.findByIdAndUpdate (courseId, reqBody, {new:true}).then ((result, error) => {
		return result
	})
}





